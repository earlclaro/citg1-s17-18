package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.CourseRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService{

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

        public void createCourse(String stringToken, Course course){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Course newCourse = new Course();
        newCourse.setName(course.getName());
        newCourse.setDescription(course.getDescription());
        newCourse.setPrice(course.getPrice());
        newCourse.setUser(author);
        courseRepository.save(newCourse);
    }

    public Iterable<Course> getCourses(){
        return courseRepository.findAll();
    }

}
